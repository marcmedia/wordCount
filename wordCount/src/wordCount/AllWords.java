package wordCount;

import java.util.ArrayList;
import java.util.List;

public class AllWords {
	
	private List<Word> allWords = new ArrayList<Word>();
	
	
	public void add(String givenWord) {
		
		Word newWord = new Word(givenWord);
		boolean exits = false;
		
		for (int i = 0; i < allWords.size(); i++) {
			if (newWord.compareTo(allWords.get(i)) == 0) {
				allWords.get(i).inc();
				exits = true;
				break;
			} 
		}
		if (!exits) {
			allWords.add(newWord);
		}
	}
	
	
	public int distinctWords() {
		return allWords.size();
	}
	
	
	public int totalWords() {
		int wordsInText = 0;
		for (int i = 0; i < this.allWords.size(); i++) {
			wordsInText += allWords.get(i).count();
		}
		return wordsInText;
	}
	
	
	@Override
	public String toString() {
		String words = "";
		for (int i = 0; i < this.allWords.size(); i++) {
			words += this.allWords.get(i).toString();
		}
		return words;
	}
}

















